#!/usr/bin/env python

#Imports
import torch

#Functions
#Create the Autoencoder Class
class AutoEncoder(torch.nn.Module):
    def __init__(self, input_dim, nlayers, latent):
        super().__init__()
        delta = int((input_dim - latent) / (nlayers + 1))

        #Encoder
        encoder = []
        nunits = input_dim
        for layer in range(nlayers):
            encoder.append(torch.nn.Linear(nunits, nunits - delta))
            encoder.append(torch.nn.ReLU())
            nunits = nunits - delta
        self.encoder = torch.nn.Sequential(*encoder)

        #Latent View
        self.lv = torch.nn.Sequential(
            torch.nn.Linear(nunits, latent),
            torch.nn.Sigmoid())

        #Decoder
        decoder = []
        nunits = latent
        for layer in range(nlayers):
            decoder.append(torch.nn.Linear(nunits, nunits + delta))
            decoder.append(torch.nn.ReLU())
            nunits = nunits + delta
        self.decoder = torch.nn.Sequential(*decoder)

        #Output
        self.output_layer = torch.nn.Sequential(
            torch.nn.Linear(nunits, input_dim),
            torch.nn.Sigmoid())

    def forward(self, x):
        encoded = self.encoder(x)
        latent_space = self.lv(encoded)
        decoded = self.decoder(latent_space)
        output = self.output_layer(decoded)
        return (latent_space, output)
