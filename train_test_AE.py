#!/usr/bin/env python

#Imports
import torch
import numpy as np
import argparse
from model import AutoEncoder

#Main Function
def main():
    """
    Main execution of the Python file as a script
    """
    #Set script arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--partition', metavar='N', default=0.8,
                        type=float,
                        help="Percentage selected of data as the training"
                        "partition.")
    parser.add_argument('-bs', '--batch-size', metavar='N', default=100,
                        type=int,
                        help="Number of samples of the dataset per batch to"
                        "load. It is the number of training examples utilized"
                        "in one iteration")
    parser.add_argument('-l', '--latent', metavar='N', default=3,
                        type=int,
                        help="Number of dimensions of the latent space. It is"
                        "a representation of compressed data in a"
                        "low-dimensional space.")
    parser.add_argument('-nl', '--number-layers', metavar='N', required=True,
                        type=int,
                        help="Number of layers in both encoder and decoder"
                        "neural networks, excluding the layer of the latent"
                        "space and the output layer.")
    parser.add_argument('-lr', '--learning-rate', metavar='N', required=True,
                        type=float,
                        help="Learning rate used for the Adam optimizer."
                        "It is the parameter in an optimization algorithm that"
                        "determines the step size at each iteration while"
                        "moving toward a minimum of the loss function.")
    parser.add_argument('-e', '--epochs', metavar='N', required=True,
                        type=int,
                        help="Number of epochs required for training the model"
                        ". It is the number times that the learning algorithm"
                        "will work through the entire training dataset.")

    #Parse arguments
    args = parser.parse_args()
    partition = args.partition
    batch_size = args.batch_size
    latent = args.latent
    nlayers = args.number_layers
    lr = args.learning_rate
    epochs = args.epochs

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    sys.stdout.write('# Using %s device.\n' % (device))

    #load the input data
    data = np.load('input_data/AE_normalized_input_data.npy')
    #select the data for training and testing
    select_train = int(partition*data.shape[0])
    select_test = int((1-partition)*data.shape[0])
    train = data[:select_train, :]
    test = data[-select_test:, :]
    #create torch tensors from numpy arrays
    train_data = torch.FloatTensor(train).to(device)
    test_data = torch.FloatTensor(test).to(device)
    #transform into a dataloaders
    train_dataloader = torch.utils.data.DataLoader(dataset = train_data,
                                                   batch_size = batch_size,
                                                   drop_last=True,
                                                   shuffle = True)
    test_dataloader = torch.utils.data.DataLoader(dataset = test_data,
                                                  batch_size = batch_size,
                                                  drop_last=True,
                                                  shuffle = False)
    sys.stdout.write('# Input data for training and testing have been loaded as'
                     ' DataLoader objects.\n')

    #call the Autoencoder
    model = AutoEncoder(input_dim = train.shape[1], nlayers = nlayers-1,
                        latent = latent)
    model = model.to(device)

    #define the optimizer and the loss function
    optimizer = torch.optim.Adam(model.parameters(), lr = lr)
    loss_function = torch.nn.MSELoss()

    #define the loop for training and testing the model
    train_losses = []
    latent_space = []
    outputs = []
    test_losses = []
    for epoch in range(1, epochs+1):
        #training step
        model.train()
        train_loss = 0
        epoch_train_losses = []
        for training_index, training_data in enumerate(train_dataloader, 1):
            training_latent, training_output = model(training_data)
            training_loss = loss_function(training_output, training_data)
            optimizer.zero_grad()
            training_loss.backward()
            optimizer.step()
            epoch_train_losses.append(training_loss.item())
            train_loss += training_loss.item()
            sys.stdout.write('\repoch %3d/%3d batch %3d/%3d\n' % (epoch,
                             epochs, training_index, len(train_dataloader)))
            sys.stdout.write('\rtrain_loss %6.3f\n'
                             % (train_loss / training_index))
            sys.stdout.flush()
        epoch_train_loss = np.mean(epoch_train_losses)
        train_losses.append(epoch_train_loss)
        #testing step
        model.eval()
        test_loss = 0
        epoch_test_losses = []
        for testing_index, testing_data in enumerate(test_dataloader, 1):
            testing_latent, testing_output = model(testing_data)
            if epoch == (epochs-1):
                latent_space.append(testing_latent.detach().cpu().numpy())
                outputs.append(testing_output.detach().cpu().numpy())
            testing_loss = loss_function(testing_output, testing_data)
            epoch_test_losses.append(testing_loss.item())
            test_loss += testing_loss.item()
            sys.stdout.write('\rbatch %3d/%3d\n' % (testing_index,
                             len(test_dataloader)))
            sys.stdout.write('\rtest_loss %6.3f\n'
                             % (test_loss / testing_index))
            sys.stdout.flush()
        epoch_test_loss = np.mean(epoch_test_losses)
        test_losses.append(epoch_test_loss)

#Main execution
if __name__ == '__main__':
    test_paths()
    main()
